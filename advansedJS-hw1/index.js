'use strict'
class Employee {
    constructor (name, age, salary){
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name () {
        return this._name
    }
    set name (value) {
        this._name = value;
    }
    get age () {
        return this._age
    }
    set age (value) {
        this._age = value;
    }
    get salary () {
        return this._salary
    }
    set salary (value) {
        this._salary = value;
    }
}

class Programmer extends Employee {
    constructor (name, age, salary, lang){
        super (name, age, salary);
        this._lang = lang;
    }
    get salary () {
        return this._salary * 3;
    }
}

const programmer1 = new Programmer('Vik', 30, 40000, ['eng', 'ua']);
console.log(programmer1);
console.log(programmer1.salary);

const programmer2 = new Programmer('Roman', 60, 10000,'ua');
console.log(programmer2);
console.log(programmer2.salary);