const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

function createList(arr, root) {
    const list = document.createElement('ul')
    arr.forEach(value => {
        try {
            if (!value.author){
                throw new Error('Не вказаний автор')
            }
            if (!value.name){
                throw new Error('Не вказана назва книги')
            }
            if (!value.price){
                throw new Error('Не вказана ціна книги')
            }
            const li = document.createElement('li');
            li.innerText = `Книга за авторства ${value.author} - "${value.name}". Ціна = ${value.price} грн`;
            list.append(li)
        }
        catch (error) {
            console.log(error.message)
        }
    })
    root.append(list);
}

createList(books, document.getElementById('root'));