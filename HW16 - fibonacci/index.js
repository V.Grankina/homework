let userNumber;
do {userNumber = +prompt('Your number?', userNumber)
} while (isNaN(userNumber) || userNumber === "" || userNumber === null);

function fibonacci(f0 = 0 , f1 = 1, n) {
    if (n === 0) {
        return f0;
    }
    if (n === 1) {
        return f1;
    }
    if  (n > 0){
        return fibonacci(f0, f1, n - 2) + fibonacci(f0, f1, n - 1);
     }
    if ( n < 0){
        return fibonacci(f0, f1, n + 2) - fibonacci(f0, f1, n + 1)
    }
}

alert(`Your fibonacci number is ${fibonacci(2,4, userNumber)}`);