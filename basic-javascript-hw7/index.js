'use strict'

function filterBy(array,typeOfDeletedData){
    let arr = (typeOfDeletedData === 'null') ? array.filter (value => value !== null) :
        (typeOfDeletedData === 'string' || typeOfDeletedData === 'number' || typeOfDeletedData === 'undefined') ? array.filter (value => typeof value !== typeOfDeletedData) :
            (typeOfDeletedData === 'array') ? array.filter (value => !Array.isArray(value)) :
                (typeOfDeletedData === 'object') ? array.filter (value => Object.prototype.toString.call(value) !== '[object Object]'):
                    'unknown type of data';
    return arr;
}

console.log(filterBy(['hello', 'world', 23, '23', null, [], {i:1}, undefined], 'array'));