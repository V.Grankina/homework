'use strict'
class StarWars {
    constructor(URL, root) {
        this.URL = URL;
        this.root = root;
    }
    queryFilms(link) {
        return fetch(link, {
            method: "GET",
            headers: {
                "Content-Type": "application/json"
            }})
            .then(response => response.json())
            .catch(error => console.error(error))
    }
    render() {
        this.queryFilms(this.URL)
            .then((response) => {
                const data = response;
                const list = document.createElement('ol');
                data.forEach(({episodeId, name, openingCrawl,characters}) => {
                    const li = document.createElement('li');
                    li.textContent = `Episode #${episodeId} - ${name}. ${openingCrawl}`;
                    const loader = document.createElement('p');
                    loader.classList.add('loader');
                    list.append(li, loader);

                    let charactersPromises = [];
                    characters.forEach((value) => {
                        const character = this.queryFilms(value);
                        charactersPromises.push(character);
                    })
                    Promise.all(charactersPromises)
                        .then(response => {
                            loader.remove();
                            const charactersList = document.createElement('div');
                            charactersList.textContent = 'Characters list:';
                            response.forEach(({name}) => charactersList.textContent += name + '; ');
                            li.append(charactersList);
                        })
                })
                this.root.append(list)
            })
    }
}

const films = new StarWars ('https://ajax.test-danit.com/api/swapi/films', document.getElementById('root'));
films.render()