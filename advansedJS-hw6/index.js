'Use strict';
async function getIp (){
    const response = await fetch('https://api.ipify.org/?format=json');
    const {ip} = await response.json();
    return ip;
}
async function getAdress (root){
    const query = await getIp();
    const response = await fetch(`http://ip-api.com/json/${query}`);
    const {country, regionName, city} = await response.json();
    const text = document.createElement('p');
    text.id = 'text';
    text.innerText = `You are in the ${country}, region - ${regionName}, city - ${city}`;
    root.after(text)
}
const button = document.getElementById('button');
button.onclick = function (){getAdress (button)};

