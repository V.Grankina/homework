'use strict'

function slider (sSelector){
    const arrOfImages = [...document.querySelectorAll(sSelector)];
    const buttonsWrapper = document.createElement('div');
    const stop = document.createElement('button');
    stop.textContent = 'Припинити';
    const contin = document.createElement('button');
    contin.textContent = 'Відновити показ';
    const countdown = document.createElement('p');
    countdown.textContent = 'Наступна картинка через';
    countdown.style.textAlign = 'center';
    buttonsWrapper.style.cssText = 'display: flex; justify-content: center; margin-top: 30px';
    document.body.append(buttonsWrapper);
    buttonsWrapper.append(stop);
    buttonsWrapper.append(contin);
    document.body.append(countdown);
    let sliding = false;
    let interval;
    let counter;
    let date;




    this.startSliding =  function (){
        if (sliding) return;
        if (arrOfImages.every(value => $(value).is(':hidden'))){
            date = new Date();
            date.setSeconds(date.getSeconds()+3);
            counter = setInterval(timer,1);
            $(arrOfImages[0]).fadeIn(500);
        } else {
            counter = setInterval(timer,1);
            changeImage()
        }

        interval = setInterval (changeImage, 3000);
        sliding = true;

    }
    contin.onclick = this.startSliding;
    stop.onclick = function () {
        clearInterval(interval);
        clearInterval(counter);
        sliding = false;
    }

    function changeImage (){
        clearInterval(counter);
        let index = arrOfImages.findIndex(value => $(value).is(':visible'))
        let fadeInterval = 500;
        $(arrOfImages[index]).fadeOut(fadeInterval, function (){
            if (index+1 === arrOfImages.length){
                $(arrOfImages[0]).fadeIn(fadeInterval);
            } else {
                $(arrOfImages[index+1]).fadeIn(fadeInterval);
            }
        });
        date = new Date();
        date.setSeconds(date.getSeconds()+3);
        counter = setInterval(timer,1);

    }
    function timer (){

        const timeDiff = date - new Date();
        const seconds = new Date(timeDiff).getSeconds();
        const miliseconds = (new Date(timeDiff).getMilliseconds()/10).toFixed(0);
        countdown.textContent = `Наступна картинка через ${seconds}:${miliseconds}`;
    }

}

let startSlider = new slider('.image-to-show');

onload = function (){
    startSlider.startSliding();
}

// var expires = new Date(); expires.setSeconds(expires.getSeconds() + 60);
// // set timer to 60 seconds var counter = setInterval(timer, 1);
// function timer() {
//     var timeDiff = expires - new Date();
//     if (timeDiff <= 0) { clearInterval(counter);
//         document.getElementById('timer').innerHTML = '00:00'; return; }
//     var seconds = new Date(timeDiff).getSeconds();
//     var milliSeconds = (new Date(timeDiff).getMilliseconds()/10).toFixed(0);
//     var seconds = seconds < 10 ? '0' + seconds : seconds;
//         var milliSeconds = milliSeconds < 10 ? '0' + milliSeconds : milliSeconds;
//         document.getElementById('timer').innerHTML = seconds + ':' + milliSeconds;


