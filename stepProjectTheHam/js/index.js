'use strict'
const servicesList = document.querySelector('.services-list');
const serviceLinks = document.querySelectorAll('.service-link');
const serviceDescriptions = document.querySelectorAll('.service-description');
let activeService = serviceLinks[0];

servicesList.addEventListener('click', (event)=> {
    event.preventDefault();
    const target = event.target;
    if (event.target.classList.contains('service-link-active')) return;
    activeService.classList.remove('service-link-active');
    activeService = event.target;
    target.classList.add('service-link-active');
    serviceDescriptions.forEach(value => {
        value.classList.remove('service-description-active');
        if (value.dataset.serviceName === target.textContent){
            value.classList.add('service-description-active')
        }
    })
})

const ourWorkList = document.querySelector('.our-work-list');
const workNames = document.querySelectorAll('.our-work-name');
const ourWorkImages = document.querySelector('.our-work-images');
const webDesignCard = document.querySelector('[data-work-type = "Web Design"]');
const graphicDesignCard = document.querySelector('[data-work-type = "Graphic Design"]');
const landingPagesCard = document.querySelector('[data-work-type = "Landing Pages"]');
const wordpressCard = document.querySelector('[data-work-type = "Wordpress"]');
const button1 = document.getElementById('load-button1');
const arrOfGraphDesignImages = ['./img/graphicDesign/graphic-design1.jpg','./img/graphicDesign/graphic-design2.jpg', './img/graphicDesign/graphic-design3.jpg', './img/graphicDesign/graphic-design4.jpg',
    './img/graphicDesign/graphic-design5.jpg', './img/graphicDesign/graphic-design6.jpg', './img/graphicDesign/graphic-design7.jpg', './img/graphicDesign/graphic-design8.jpg',
    './img/graphicDesign/graphic-design9.jpg', './img/graphicDesign/graphic-design10.jpg', './img/graphicDesign/graphic-design11.jpg', './img/graphicDesign/graphic-design12.jpg',
    './img/graphicDesign/graphic-design13.jpg', './img/graphicDesign/graphic-design14.jpg', './img/graphicDesign/graphic-design15.jpg', './img/graphicDesign/graphic-design16.jpg',
    './img/graphicDesign/graphic-design17.jpg', './img/graphicDesign/graphic-design18.jpg', './img/graphicDesign/graphic-design19.jpg', './img/graphicDesign/graphic-design20.jpg',
    './img/graphicDesign/graphic-design21.jpg', './img/graphicDesign/graphic-design22.jpg', './img/graphicDesign/graphic-design23.jpg', './img/graphicDesign/graphic-design24.jpg']
const arrOfLandingPageImages = [`./img/landingPage/landing-page1.jpg`, `./img/landingPage/landing-page2.jpg`, `./img/landingPage/landing-page3.jpg`, `./img/landingPage/landing-page4.jpg`,
    `./img/landingPage/landing-page5.jpg`,  `./img/landingPage/landing-page6.jpg`, `./img/landingPage/landing-page7.jpg`,`./img/landingPage/landing-page8.jpeg`, `./img/landingPage/landing-page9.jpeg`];
const arrOfWebDesignImages = [`./img/webDesign/web-design1.jpg`,`./img/webDesign/web-design2.jpg`, `./img/webDesign/web-design3.jpg`,
    `./img/webDesign/web-design4.jpg`,`./img/webDesign/web-design5.jpg`,`./img/webDesign/web-design6.jpg`, `./img/webDesign/web-design7.jpg`, `./img/webDesign/web-design8.jpeg`, `./img/webDesign/web-design9.jpeg`,];
const arrOfWordpressImages = ['./img/wordpress/wordpress1.jpg', './img/wordpress/wordpress2.jpg','./img/wordpress/wordpress3.jpg', './img/wordpress/wordpress4.jpg', './img/wordpress/wordpress5.jpg',
    './img/wordpress/wordpress6.jpg', './img/wordpress/wordpress7.jpg', './img/wordpress/wordpress8.jpg', './img/wordpress/wordpress9.jpg', './img/wordpress/wordpress10.jpg']
let i = 1;
let activeWorkName = workNames[0];
let activeGallery;
let maxImages = 12;

uploadAllImages (i);
activeGallery = 'All';
i = 12;
maxImages = 24;

ourWorkList.addEventListener('click', (event)=> {
    maxImages = 12;
    const target = event.target;
    if (target.classList.contains('our-work-name-active')) return;
    activeWorkName.classList.remove('our-work-name-active');
    activeWorkName = event.target;
    target.classList.add('our-work-name-active');
    ourWorkImages.innerHTML='';
    i = 1;
    if (target.textContent === 'All'){
        uploadAllImages (i);
        activeGallery = 'All';
    }
    if (target.textContent ==='Graphic Design'){
        uploadGraphicDesignImages(i);
        activeGallery = 'Graphic Design';
    }
    if (target.textContent === 'Web Design'){
        uploadWebDesignImages(i);
        activeGallery = 'Web Design';
    }
    if (target.textContent === 'Landing Pages'){
        uploadLandingPagesImages(i);
        activeGallery = 'Landing Pages';
    }
    if (target.textContent === 'Wordpress'){
        uploadWordpressImages(i);
        activeGallery = 'Wordpress';
    }
    if (ourWorkImages.children.length < maxImages ){
        button1.remove();
    } else {
        ourWorkImages.after(button1);
    }
    i = 12;
    maxImages = 24;
    })


function uploadAllImages (i){
    if (i === 1) {
        ourWorkImages.append(webDesignCard);
        ourWorkImages.append(graphicDesignCard);
        ourWorkImages.append(landingPagesCard);
        ourWorkImages.append(wordpressCard);
    }
    if  (i > 1) { i = i /4}
    maxImages = maxImages/4;
    for (i; i < maxImages; i++){
        const newWebDesignCard = webDesignCard.cloneNode(true);
        newWebDesignCard.firstElementChild.src = arrOfWebDesignImages[i];
        ourWorkImages.append(newWebDesignCard);

        const newGraphicDesignCard = graphicDesignCard.cloneNode(true);
        newGraphicDesignCard.firstElementChild.src = arrOfGraphDesignImages [i];
        ourWorkImages.append(newGraphicDesignCard);

        const newLandingPagesCard = landingPagesCard.cloneNode(true);
        newLandingPagesCard.firstElementChild.src = arrOfLandingPageImages[i];
        ourWorkImages.append(newLandingPagesCard);

        const newWordpressCard = wordpressCard.cloneNode(true);
        newWordpressCard.firstElementChild.src = arrOfWordpressImages[i];
        ourWorkImages.append(newWordpressCard);
    }
}
function uploadWebDesignImages(i){
    ourWorkImages.append(webDesignCard);
    for (i; i < maxImages && i < arrOfWebDesignImages.length; i++){
        const newWebDesignCard = webDesignCard.cloneNode(true);
        newWebDesignCard.firstElementChild.src = arrOfWebDesignImages[i];
        ourWorkImages.append(newWebDesignCard);}
}
function uploadGraphicDesignImages(i){
    ourWorkImages.append(graphicDesignCard);
    for (i; i < maxImages && i < arrOfGraphDesignImages.length; i++){
        const newGraphicDesignCard = graphicDesignCard.cloneNode(true);
        newGraphicDesignCard.firstElementChild.src = arrOfGraphDesignImages [i];
        ourWorkImages.append(newGraphicDesignCard);}
}
function uploadLandingPagesImages(i){
    ourWorkImages.append(landingPagesCard);
    for (i; i < maxImages && i < arrOfLandingPageImages.length; i++){
        const newLandingPagesCard = landingPagesCard.cloneNode(true);
        newLandingPagesCard.firstElementChild.src = arrOfLandingPageImages[i];
        ourWorkImages.append(newLandingPagesCard);}

}
function uploadWordpressImages(i){
    ourWorkImages.append(wordpressCard);
    for (i; i < arrOfWordpressImages.length && i < maxImages; i++){
        const newWordpressCard = wordpressCard.cloneNode(true);
        newWordpressCard.firstElementChild.src = arrOfWordpressImages[i];
        ourWorkImages.append(newWordpressCard);
    }
}

button1.onclick = function (){
    button1.remove();
    const loader = document.createElement('div');
    loader.classList.add('loader');
    ourWorkImages.after(loader);
    setTimeout(addImages, 1000)
    function addImages () {
        loader.remove()
        if (activeGallery === 'All') {
            uploadAllImages(i);
        }
        if (activeGallery === 'Graphic Design') {
            uploadGraphicDesignImages(i);
        }
        if (activeGallery === 'Web Design') {
            uploadWebDesignImages(i);
        }
        if (activeGallery === 'Landing Pages') {
            uploadLandingPagesImages(i);
        }
        if (activeGallery === 'Wordpress') {
            uploadWordpressImages(i);
        }
        if (ourWorkImages.children.length !== 36 && (activeGallery === 'All' || (activeGallery === 'Graphic Design' && arrOfGraphDesignImages.length > maxImages) || (activeGallery === 'Landing Pages' && arrOfLandingPageImages.length > maxImages) ||
            (activeGallery === 'Wordpress' && arrOfWordpressImages.length > maxImages) || (activeGallery === 'Web Design' && arrOfWebDesignImages.length > maxImages))) {
            ourWorkImages.after(button1);
        }
        i = 24;
        maxImages = 36;
    }
}



const galleryThumbs = new Swiper('.gallery-thumbs', {
    spaceBetween: 30,
    slidesPerView: 4,
    loop: true,
    freeMode: true,
    loopedSlides: 5,
    watchSlidesProgress: true,

});
const galleryTop = new Swiper('.gallery-top', {
   loop: true,
   loopedSlides: 5,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
    thumbs: {
        swiper: galleryThumbs,
    },
});
//
    let $gallerywrapper = $('.gallery-wrapper').imagesLoaded(function () {
        $gallerywrapper.masonry({
            columnWidth: 10,
            itemSelector: '.gallery-item',
            gutter: 6,
        });
        $('.gallery-wrapper1').masonry({
            itemSelector: '.gallery-item1',
            gutter: 3
        });

    });


const galleryWraper = document.querySelector('.gallery-wrapper');
const button2 = document.getElementById('load-button2');
const galleryItem = document.querySelector('.gallery-item');

button2.onclick = function (){
    button2.remove();
    const loader = document.createElement('div');
    loader.classList.add('loader');
    galleryWraper.after(loader);
    setTimeout(addImages, 1000);
    function addImages () {
        loader.remove();
        for (let i = 1; i < 7; i++) {
            const newImg = galleryItem.cloneNode(true);
            newImg.firstElementChild.src = `./img/masonry/masonry${i}.jpg`;
            $gallerywrapper.append(newImg).masonry( 'appended', newImg );
        }
    }
}
