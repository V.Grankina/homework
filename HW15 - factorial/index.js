'use strict'

let userNumber;
do {userNumber = prompt('Your number?', userNumber)
} while (isNaN(userNumber) || userNumber === "" || userNumber === null);

alert(`Factorial of ${userNumber} is ${factorialOf(userNumber)}`);

 function factorialOf(x){
     if (x===1){
         return 1;
     }
     return x * factorialOf(x-1);
 }