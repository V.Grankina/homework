'Use strict'

class Request {
    getData(URL) {
        return fetch(URL)
            .then(response => response.json())
            .catch(error => console.error(error))
    }
}

class Post {
    post(json) {
        return fetch("https://ajax.test-danit.com/api/json/posts/", {
            method: 'POST',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
            .catch(error => console.log(error))
    }
}

class Patch {
    patch(id, json) {
       return fetch("https://ajax.test-danit.com/api/json/posts/" + id, {
            method: 'PATCH',
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        })
            .then(response => response.json())
    }
}

const request = new Request();
const newPost = new Post();
const patch = new Patch;


class ModalWindow {
    constructor(modalWindow) {
        this.modalWindow = modalWindow
    }

    title = document.getElementById('title');
    text = document.getElementById('text');
    form = document.querySelector('.modal__form');

    open(forWhat) {
        this.modalWindow.style.display = 'block';
        this.title.placeholder = "Title";
        this.text.placeholder = "Add some text here";
        const closeBtn = this.modalWindow.querySelector('.form__discard');
        const submitBtn = this.modalWindow.querySelector('#change');
        const createBtn = this.modalWindow.querySelector('#create');
        closeBtn.addEventListener('click', (ev) => {
            ev.preventDefault();
            this.close();
        })
        if (forWhat === 'create') {
            submitBtn.style.display = 'none';
            createBtn.style.display = 'inline-block'
        } else {
            submitBtn.style.display = 'inline-block';
            createBtn.style.display = 'none'
        }

    }

    close() {
        this.modalWindow.style.display = 'none';
    }

    submit(evTarget) {
        const formElements = this.formCheck(evTarget);
        if (formElements) {
            this.close();
            const newTitle = formElements[0].value;
            const newText = formElements[1].value;
            this.form.reset();
            return {
                body: newText,
                title: newTitle,
            }
        }
        alert('Fill all fields');
    }

    formCheck(form) {
        const formElements = [...form.elements].filter(({type}) => type !== 'submit');
        const check = formElements.some(({value}) => value.trim() === '');
        return check ? false : formElements;
    }
}

class Card {
    constructor(title, text, user, email, postId) {
        this.title = title;
        this.text = text;
        this.user = user;
        this.email = email;
        this.postId = postId;
    }

    template = document.getElementById('card__template').content;

    render(root) {
        this.card = this.template.querySelector('.card').cloneNode(true);
        this.cardTitle = this.card.querySelector('.card__title');
        this.cardText = this.card.querySelector('.card__text');
        const user = this.card.querySelector('.card__user');
        const email = this.card.querySelector('.card__user-email');
        this.cardTitle.textContent = this.title;
        this.cardText.textContent = this.text;
        user.textContent = this.user;
        email.textContent = this.email;
        this.card.dataset.id = this.postId;
        root.prepend(this.card);
    }

    delete(id) {
        const deleteBtn = this.card.querySelector('.card__btn--delete');
        deleteBtn.addEventListener('click', (ev) => {
            fetch("https://ajax.test-danit.com/api/json/posts/" + id, {method: 'DELETE'})
                .then(response => {
                    if (!response.ok) {
                        throw new Error('fail to delete card')
                    }
                    ev.target.closest('.card').remove();
                })
                .catch(error => console.log(error))
        })
    }

    edit(id) {
        const editBtn = this.card.querySelector('.card__btn--edit');
        editBtn.addEventListener('click', () => {
            const modalWindow = new ModalWindow(document.querySelector('.modal'));
            modalWindow.open();
            const editPost = (ev) => {
                ev.preventDefault();
                const formData = modalWindow.submit (ev.target);
                if (formData) {
                    const json = JSON.stringify(formData)
                    patch.patch(id, json).then(data => {
                        const {body, title} = data;
                        this.cardTitle.textContent = title;
                        this.cardText.textContent = body;
                        modalWindow.form.removeEventListener('submit', editPost)
                    })

                }
            }
            modalWindow.form.addEventListener('submit', editPost);
        })
    }
}

class Blog {
    constructor(root) {
        this.root = root
    }

    render() {
        const users = new Request().getData('https://ajax.test-danit.com/api/json/users');
        const posts = new Request().getData('https://ajax.test-danit.com/api/json/posts');
        const loader = document.querySelector('.loader');
        Promise.all([users, posts])
            .then(data => {
                this.users = data[0].map(({id, name, email}) => ({id, name, email}));
                this.posts = data[1].map(({id, userId, title, body}) => ({id, userId, title, body}));
                this.posts.forEach(({id: postId, userId, title, body}) => {
                        const {name, email} = this.users.find(({id}) => id === userId);
                        const card = new Card(title, body, name, email, postId);
                        loader.remove();
                        card.render(this.root);
                        card.delete(postId);
                        card.edit(postId);
                    }
                )
            })
    }

    addNewPost() {
        const addBtn = document.querySelector('.header__btn');
        addBtn.addEventListener('click', (ev) => {
            const modalWindow = new ModalWindow(document.querySelector('.modal'));
            modalWindow.open('create');
            const addNewPost = (ev) => {
                ev.preventDefault();
                const formData = modalWindow.submit(ev.target);
                if (formData) {
                    const json = JSON.stringify({...formData, userId: 1})
                    newPost.post(json).then(data => {
                        const {body, title, userId, id: postId} = data;
                        const {name, email} = this.users.find(({id}) => id === userId);
                        const card = new Card(title, body, name, email, postId);
                        card.render(this.root);
                        card.delete(postId);
                        card.edit(postId);
                        modalWindow.form.removeEventListener('submit', addNewPost)
                    })

                }
            }
            modalWindow.form.addEventListener('submit', addNewPost);
        })
    }
}

const blog = new Blog(document.querySelector('#root'));
blog.render();
blog.addNewPost();


