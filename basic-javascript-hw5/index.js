'use sctrict'
// ## Завдання
//
// Реалізувати функцію створення об'єкта "юзер". Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.
//
// #### Технічні вимоги:
//
//
// - Додати в об'єкт `newUser` метод `getLogin()`, який повертатиме першу літеру імені юзера, з'єднану з прізвищем, все в нижньому регістрі (наприклад, `Ivan Kravchenko → ikravchenko`).
// - Створити юзера за допомогою функції createNewUser(). Викликати у цього юзера функцію `getLogin()`. Вивести у консоль результат виконання функції.
//
// #### Необов'язкове завдання підвищеної складності
//
// - Зробити так, щоб властивості `firstName` та `lastName` не можна було змінювати напряму. Створити функції-сеттери `setFirstName()` та `setLastName()`, які дозволять змінити дані властивості.


// Створюємо нового юзера за допомогою функції
const newUser = createNewUser ();
// Перевіряємо чи норм створився
console.log(newUser);
// Перевірєм чи працює getLogin
console.log(newUser.getLogin());
// Змінюємо імʼя
newUser.firstName = "Javelina";
newUser.firstName = "";
newUser.firstName = "123";
// Перевірєм чи працює getLogin з новим іменем
console.log(newUser.getLogin());

// Функція яка створює нового юзера, вже має закладені сетери для firstName та lastName:
function createNewUser(){
    let user = {
        set firstName (value){
            if (value.match(/^[a-zA-z]+$/i)){
                this._firstName = value;
            }
        },
        get firstName() {return this._firstName},
        set lastName (value){
            if (value.match(/^[a-zA-z]+$/i)){
                this._lastName = value;
            }
        },
        get lastName() {return this._lastName},
        getLogin () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        }

    }
    do {user._firstName = prompt("What is your name?", 'Victoria')}
    while (!user._firstName);
    do {user._lastName = prompt("What is your surname?", 'Grankina')}
    while (!user._lastName);
    return user;
}

