'use strict';
const buttons = [...document.querySelectorAll('button')];
// Цього разу без обгортки не працює метод buttons.find, але раніше дійсно обготрка [... була зайвою

document.addEventListener('keydown', (event) =>{
    let key = event.code;
    if (key.includes('Key')){
        key = key.slice(3)
    }
    const target = buttons.find(value => value.textContent === key);
    if (target){
        buttons.forEach(value => value.removeAttribute('data-blue'))
        target.dataset.blue = '';
    }
})
