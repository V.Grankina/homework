'use strict'

const newUser = createNewUser ();
console.log(newUser);
console.log (newUser.getAge());
console.log(newUser.getPassword());

function createNewUser(){
    let user = {
        set firstName (value){
            if (value.match(/^[a-zA-z]+$/i)){
                this._firstName = value;
            }
        },
        get firstName() {return this._firstName},
        set lastName (value){
            if (value.match(/^[a-zA-z]+$/i)){
                this._lastName = value;
            }
        },
        get lastName() {return this._lastName},
        getLogin () {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge () {
            let birthday = new Date(this.birthday.slice(6) + '-' + this.birthday.slice(3,5) + '-' + this.birthday.slice(0,2));
            let today = new Date ();
            let m = today.getMonth() - birthday.getMonth();
            let d = today.getDay() - birthday.getDay();
            let age = today.getFullYear() - birthday.getFullYear();
            if (m < 0) {age--};
            if (m === 0 && d < 0) {age--};
            return age;
        },
        getPassword(){
            return this.firstName[0].toUpperCase() + this.firstName.slice(1).toLowerCase()+this.birthday.slice(6);
        }

    }
    do {user._firstName = prompt("What is your name?", 'Victoria')}
    while (!user._firstName);
    do {user._lastName = prompt("What is your surname?", 'Grankina')}
    while (!user._lastName);
    do {user.birthday = prompt("What is your birthday?", '19.03.1992')}
    while (!user.birthday);
    return user;
}

