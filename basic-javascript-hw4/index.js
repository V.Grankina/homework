
// ## Завдання
// Отримати за допомогою модального вікна браузера два числа.
let userNumber1;
let userNumber2;
let userOperator;

do {userNumber1 = +prompt('Your number1?', userNumber1)
} while (isNaN(userNumber1) || userNumber1 === "" || userNumber1 === null);
do {userNumber2 = +prompt('Your number2?', userNumber2)
} while (isNaN(userNumber2) || userNumber2 === "" || userNumber2 === null);
do {userOperator = prompt('Operation?')
} while (userOperator !== '+' && userOperator !== '-' && userOperator !== '*' && userOperator !== '/')

console.log(calculator(userNumber1, userNumber2 , userOperator));


// функція калькулятор двох чисел з оператором о
function calculator (n1, n2, o) {
    if (n2 === 0 && o === '/'){  // проверка не делим ли на ноль
        return 'you cant divide by zero';
    }
    switch (o) {
        case '-':
            return n1 - n2;
            break;
        case '+':
            return  n1+n2;
            break;
        case '*':
            return  n1*n2;
            break;
        case '/':
            return n1/n2;
            break
    }
}