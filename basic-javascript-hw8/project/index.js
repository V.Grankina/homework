'use strict'
let paragraph = [...document.querySelectorAll('p')];
paragraph.forEach(value => value.style.backgroundColor = "#ff0000");

const optionsList = document.getElementById('optionsList');
console.log(optionsList);
console.log(optionsList.parentElement);
optionsList.childNodes.forEach(value => console.log(value.nodeName, value.nodeType));

const testParagraph = document.getElementById('testParagraph');
testParagraph.innerText = "This is a paragraph";

const allLi = [...document.querySelectorAll('.main-header li')];
console.log(allLi);
allLi.forEach(value => value.classList.add ('nav-item'));

const optionList = [...document.querySelectorAll('.options-list-title')];
console.log(optionList);
optionList.forEach(value => value.classList.remove('options-list-title'));
console.log(document.querySelector('script').innerText)
