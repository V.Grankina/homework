'use strict'
const prevBtn = document.querySelector('.prev')
const nextBtn = document.querySelector('.next')
const outImg = document.querySelector('.slider__img')

const art1 = './cat1.jpeg'
const art2 = './cat2.jpeg'
const art3 = './cat3.jpeg'


const images = [art1, art2, art3]

let i = 0
outImg.innerHTML =`<img src=${images[i]}>`

nextBtn.onclick = () =>{
    if(i < images.length - 1){
        i++
    }else{
        i = 0
    }
    outImg.innerHTML =`<img src=${images[i]}>`
}

prevBtn.onclick = () =>{
    if(i > 0 ){
        i--
    }else{
        i = images.length-1
    }
    outImg.innerHTML =`<img src=${images[i]}>`
}