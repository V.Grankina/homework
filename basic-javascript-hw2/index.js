/* Теоретичні питання
1. Какие существуют типы данных в Javascript?
number + (bigint); boolean; string; null; undefined; object; symbol.
var let cost

2. В чем разница между == и === ?

==  сравнивает с приведением типов (false == null == 0 == NaN);
=== сравнивает без приведения типов (0 === 0)

3. Что такое оператор?
 внутренняя функция JS
- математические операторы ( + - * ** / % ++ --)
- операторы присваивания ( = +=, -=, *=, /=, %=, **=, <<=, >>=, >>>=, &=, ^=, |=)
- операторы сравнения ( == === != !== > < >= <=)
- оператор конкатинации ( +)
- логические операторы ( && || )

 */
// ## Завдання
 let userAge;
 let userName;

do { userAge = prompt('Your age?', userAge)
} while (isNaN(userAge) || userAge === "" || userAge === null);

do {userName = prompt('Your name?', userName)
} while (
  !userName.match(/^[a-zA-z]+$/i) || userName === "" || userName === null || userName === 'null'
    )
console.log(userName);
if (checkAge(userAge)){
    alert (`Welcome ${userName}`);
} else {
    alert(`You are not allowed to visit this website`);
}
 function checkAge(x){
     if (x < 18) {
        return false
     }
     if ( x >= 18 && x <= 22){
        let userConfirm = confirm('Are you sure you want to continue?');
         return userConfirm;
     } else {
         return true
     }
 }