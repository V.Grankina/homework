'use strict'
const start = document.getElementById('start');

start.onclick = (event) => {
    start.remove();
    const game = new Game();
    game.render(document.querySelector('body'));
    game.play();
}

class Game {
    constructor() {
        do {
            this.level = prompt('Choose a level: easy, medium or hard?', 'hard')
        }
        while (this.level !== 'easy' && this.level !== 'medium' && this.level !== 'hard');
    }

    playingField = [];
    playerCount = 0;
    computerCount = 0;

    render(root) {
        const score = document.createElement('div');
        score.classList.add('score-table');
        const scoreTitle = document.createElement('h1');
        scoreTitle.innerText = 'РАХУНОК';
        const player = document.createElement('div');
        const playerText = document.createElement('p');
        playerText.innerText = 'Гравець';
        this.playerScore = document.createElement('div');
        const computer = document.createElement('div');
        const computerText = document.createElement('p');
        computerText.innerText = 'Фемпʼютер';
        this.computerScore = document.createElement('div');
        player.append(playerText, this.playerScore);
        computer.append(computerText, this.computerScore);
        score.append(scoreTitle, player, computer);


        this.table = document.createElement('table');
        for (let i = 0; i < 10; i++) {
            let tr = document.createElement('tr');
            for (let y = 0; y < 10; y++) {
                let td = document.createElement('td');
                td.classList.add('td--basic');
                this.playingField.push(td);
                tr.append(td)
            }
            this.table.append(tr)
        }
        root.append(score, this.table)
    }

    setTimeOut() {
        if (this.level === 'easy') {
            this.timeout = 1500;
        }
        if (this.level === 'medium') {
            this.timeout = 1000;
        }
        if (this.level === 'hard') {
            this.timeout = 500;
        }
    }

    play() {
        this.setTimeOut();
        this.playerScore.innerText = this.playerCount;
        this.computerScore.innerText = this.computerCount;
        this.intervalId = setTimeout(this.getMole, this.timeout);
        this.table.addEventListener('click', (event) => {
            let activeTD = event.target;
            if (activeTD.classList.contains('td--active')) {
                activeTD.classList.remove('td--active')
                activeTD.classList.add('td--player');
                this.playerCount++;
                this.playerScore.innerText = this.playerCount;
            }
        })
    }

    getMole = () => {
        console.log(this.intervalId);
        const randomNumber = this.getRandomInt(this.playingField.length);
        let randomTd = this.playingField[randomNumber];
        randomTd.classList.add('td--active');
        this.playingField.splice(randomNumber, 1);
        setTimeout(() => {
            if (randomTd.classList.contains('td--active')) {
                this.computerCount++;
                this.computerScore.innerText = this.computerCount;
                randomTd.classList.remove('td--active');
                randomTd.classList.add('td--computer');
            }
            if (this.playerCount > 50 || this.computerCount > 50){
                clearTimeout(this.intervalId);
                }
            this.endGame();
        }, this.timeout);
        if (this.playerCount < 50 && this.computerCount < 50){
            setTimeout(this.getMole, this.timeout);
        }

    }
    endGame() {
        if (this.playerCount >= 50 || this.computerCount >= 50 || (this.playerCount === 50 && this.computerCount === 50)) {

            if (this.playerCount > this.computerCount) {
                this.computerScore.innerText = 'looser';
                this.playerScore.innerText = 'win-win';
            }
            if (this.playerCount < this.computerCount) {
                this.computerScore.innerText = 'win-win';
                this.playerScore.innerText = 'looser';
            }
            if (this.computerCount === this.playerCount) {
                this.computerScore.innerText = 'friends';
                this.playerScore.innerText = 'forever';
            }
        }
    }
    getRandomInt(max) {
        return Math.floor(Math.random() * max);
    }
}
