'use strict'

let tabs = document.querySelectorAll('.tabs-title')

tabs.forEach((value, index,array) => {
    value.addEventListener('click', () => {
        tabs.forEach((val, ind) => {
            if (tabs[ind].dataset.type === 'active'){
                tabs[ind].dataset.type = 'deactivated'
            }
        })
        tabs[index].dataset.type = 'active';
        let tabsContent = document.querySelectorAll('.tabs-content li');
        tabsContent.forEach((v,) => {
        if (v.dataset.visibility === 'on'){
            v.dataset.visibility = 'off'
        }
        if (v.dataset.title === tabs[index].textContent){
            v.dataset.visibility = 'on';
        }
        })
    })
})
