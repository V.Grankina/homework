const form = document.querySelector('.password-form');
const password = document.getElementById('password');
const passwordConfirm = document.getElementById('password-confirm');
const icon = document.getElementById('icon');
const iconConfirm = document.getElementById('icon-confirm');
const btn = document.querySelector('.btn');
const error = document.createTextNode('Нужно ввести одинаковые значения');


icon.addEventListener('click', (event) => {
    if (password.type === 'password'){
    icon.classList.add ('fa-eye-slash');
    password.removeAttribute ('type');
    password.setAttribute('type', 'text');
    } else {
        icon.classList.remove ('fa-eye-slash');
        password.removeAttribute ('type');
        password.setAttribute('type', 'password');
    }
})
iconConfirm.addEventListener('click', (event) => {
    if (passwordConfirm.type === 'password'){
    iconConfirm.classList.add ('fa-eye-slash');
    passwordConfirm.removeAttribute ('type');
    passwordConfirm.setAttribute('type', 'text');
    } else {
        iconConfirm.classList.remove ('fa-eye-slash');
        passwordConfirm.removeAttribute ('type');
        passwordConfirm.setAttribute('type', 'password');
    }
})
form.addEventListener('submit', (event)=> event.preventDefault);

btn.addEventListener ('click', (event) => {
    if (password.value === passwordConfirm.value && password.value !== ''){
        alert ('You are welcome')
    } else {
        btn.after(error);
    }
}
)
password.addEventListener('keydown', (e)=> error.remove());
passwordConfirm.addEventListener('keydown', (e)=> error.remove());